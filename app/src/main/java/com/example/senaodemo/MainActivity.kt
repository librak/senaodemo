package com.example.senaodemo

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.ActionBar
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.senaodemo.api.ApiClientBuilder
import com.example.senaodemo.api.gson.Data
import com.example.senaodemo.api.gson.Product
import com.example.senaodemo.databinding.ActivityMainBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity(), SearchView.OnQueryTextListener {
    private lateinit var viewModel: MainActivityViewModel
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.hide()
        viewModel = ViewModelProvider(this).get(MainActivityViewModel::class.java)

        setActionBar()
        setView()

        viewModel.httpProducts()
    }

    @SuppressLint("InflateParams")
    private fun setActionBar() {
        setSupportActionBar(binding.tbToolbar)
        val customView =
            LayoutInflater.from(this).inflate(R.layout.actionbar_search, null)
        val params = ActionBar.LayoutParams(
            ActionBar.LayoutParams.MATCH_PARENT,
            ActionBar.LayoutParams.WRAP_CONTENT,
            Gravity.START or Gravity.CENTER_VERTICAL
        )
        supportActionBar?.run {
            setCustomView(customView, params)
            setDisplayHomeAsUpEnabled(false)
            setDisplayShowHomeEnabled(false)
            setDisplayShowCustomEnabled(true)
            val searchView = customView.findViewById<SearchView?>(R.id.search_view)
            val searchIcon: ImageView? = searchView?.findViewById(R.id.search_button)
            searchView?.setOnQueryTextListener(this@MainActivity)
            searchView?.setOnClickListener {
                searchIcon?.performClick()
            }
        }
    }

    private fun setView() {
        binding.rv.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        viewModel.getLiveProducts().observe(this, {
            if (it != null) {
                binding.rv.adapter = ProductAdapter(this, it)
            }
        })
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        query?.let {
            viewModel.searchMartName(it)
            Utility.hideSoftKeyboard(this)
        }
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        newText?.let {
            viewModel.searchMartName(it)
        }
        return true
    }

}

class MainActivityViewModel : ViewModel() {
    private val rep = MainActivityRep()
    private val liveProducts: MutableLiveData<ArrayList<Product>> = MutableLiveData()
    private val allProduct: ArrayList<Product> = ArrayList()

    fun httpProducts() {
        rep.httpProducts(liveProducts, allProduct)
    }

    fun getLiveProducts(): LiveData<ArrayList<Product>> {
        return liveProducts
    }

    fun searchMartName(query: String) {
        val tempList = ArrayList<Product>()
        for (prod in allProduct) {
            if (prod.martName?.lowercase()?.contains(query.lowercase()) == true) {
                tempList.add(prod)
            }
        }
        liveProducts.postValue(tempList)
    }

}

class MainActivityRep {
    fun httpProducts(liveProducts: MutableLiveData<ArrayList<Product>>, allProduct: ArrayList<Product>) {
        val call = ApiClientBuilder.createApiClient().getProducts()
        call.enqueue(object : Callback<Data> {
            override fun onResponse(call: Call<Data>, response: Response<Data>) {
                if (response.isSuccessful) {
                    response.body()?.let {
                        liveProducts.postValue(it.data)
                        allProduct.addAll(it.data)
                    }
                    Log.e("aaaaaa", "data:${Utility.convertGsonToString(response.body())}")
                } else {
                    liveProducts.postValue(null)
                }
            }

            override fun onFailure(call: Call<Data>, t: Throwable) {
                liveProducts.postValue(null)
            }

        })
    }
}