package com.example.senaodemo.api

import com.example.senaodemo.api.gson.Data
import retrofit2.Call
import retrofit2.http.*

interface ApiService {

    @Headers("Content-Type: application/json")
    @GET("apis2/test/marttest.jsp")
    fun getProducts(): Call<Data>
}