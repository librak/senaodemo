package com.example.senaodemo.api.gson

import java.io.Serializable

data class Data(var data: ArrayList<Product>)

class Product : Serializable {
    var price: Int? = null
    var martShortName: String? = null
    var imageUrl: String? = null
    var finalPrice: Int? = null
    var martName: String? = null
    var stockAvailable: String? = null
    var martId: Int? = null
}