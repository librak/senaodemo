package com.example.senaodemo

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.senaodemo.api.gson.Product
import java.text.DecimalFormat


class ProductAdapter(val activity: Activity, val list: List<Product>) :
    RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(activity).inflate(
                R.layout.item_product,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    inner class ViewHolder(private val pContentView: View) : RecyclerView.ViewHolder(pContentView) {
        private var ivProd: ImageView = pContentView.findViewById(R.id.iv_prod)
        private var tvMartName: TextView = pContentView.findViewById(R.id.mart_name)
        private var tvFinalPrice: TextView = pContentView.findViewById(R.id.final_price)

        fun bind(position: Int) {
            val prod = list[position]
            Glide.with(activity).load(prod.imageUrl).into(ivProd)
            tvMartName.text = prod.martName
            val df = DecimalFormat("#,###.##")
            tvFinalPrice.text = "$${df.format(prod.finalPrice)}"

            itemView.setOnClickListener {
                val intent = Intent(activity, ProdActivity::class.java)
                intent.putExtra("prod", prod)
                activity.startActivity(intent)
            }
        }

    }
}