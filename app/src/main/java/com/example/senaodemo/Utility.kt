package com.example.senaodemo

import android.app.Activity
import android.content.Context
import android.view.inputmethod.InputMethodManager
import com.google.gson.Gson

object Utility {
    fun convertGsonToString(gsonObj: Any?): String {
        val gson = Gson()
        return gson.toJson(gsonObj)
    }

    fun hideSoftKeyboard(activity: Activity?) {
        if (activity != null) {
            val view = activity.currentFocus
            if (view != null) {
                val imm =
                    activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view.windowToken, 0)
            }
        }
    }
}