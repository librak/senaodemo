package com.example.senaodemo

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.ImageButton
import androidx.appcompat.app.ActionBar
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.example.senaodemo.api.gson.Product
import com.example.senaodemo.databinding.ActivityProdBinding
import java.text.DecimalFormat

class ProdActivity : AppCompatActivity() {
    private lateinit var viewModel: ProdActivityViewModel
    private lateinit var binding: ActivityProdBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProdBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.hide()
        viewModel = ViewModelProvider(this).get(ProdActivityViewModel::class.java)

        viewModel.prod = intent.getSerializableExtra("prod") as? Product

        setActionBar()
        setView()
    }

    @SuppressLint("InflateParams")
    private fun setActionBar() {
        setSupportActionBar(binding.tbToolbar)
        val customView =
            LayoutInflater.from(this).inflate(R.layout.actionbar_back_title, null)
        val params = ActionBar.LayoutParams(
            ActionBar.LayoutParams.MATCH_PARENT,
            ActionBar.LayoutParams.WRAP_CONTENT,
            Gravity.START or Gravity.CENTER_VERTICAL
        )
        supportActionBar?.run {
            setCustomView(customView, params)
            setDisplayHomeAsUpEnabled(false)
            setDisplayShowHomeEnabled(false)
            setDisplayShowCustomEnabled(true)
            val btBack = customView.findViewById<ImageButton?>(R.id.btn_back)
            btBack?.setOnClickListener {
                onBackPressed()
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setView() {
        val prod = viewModel.prod
        Glide.with(this).load(prod?.imageUrl).into(binding.ivProd)

        binding.tvId.text = getString(R.string.prod_id) + prod?.martId

        binding.tvName.text = prod?.martName

        val df = DecimalFormat("#,###.##")
        binding.finalPrice.text = "$${df.format(prod?.finalPrice)}"

    }
}

class ProdActivityViewModel : ViewModel() {
    var prod: Product? = null
}